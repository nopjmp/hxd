OUTPUT=hxd
CFLAGS=-std=c99 -Wall -Wextra -Werror
DEBUG_FLAGS=-g
OPT_FLAGS=-O3
all: hxd

hxd: main.c Makefile
	${CC} main.c -o ${OUTPUT} ${OPT_FLAGS} ${CFLAGS}

clean:
	rm -f hxd

debug: main.c Makefile
	${CC} main.c -o ${OUTPUT} ${DEBUG_FLAGS} ${CFLAGS}

install:
	install ${OUTPUT} /usr/local/bin/
