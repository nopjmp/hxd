#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <getopt.h>

static struct option long_options[] = {
	{ "width",  required_argument, 0, 'w' },
	{ "offset", required_argument, 0, 'o' },
	{ "length", required_argument, 0, 'l' },
	{ "help",         no_argument, 0, 'h' },
	{ 0, 0, 0, 0 }
};

size_t g_width = 16;
size_t g_offset = 0;
size_t g_length = 0;

size_t parse_int_arg(const char* name) {
	long long int value = strtoll(optarg, NULL, 0);
	if (errno == ERANGE || value <= 0) {
		fprintf(stderr, "%s value was invalid.\n", name);
		exit(1);
	}
	return (size_t)value;
}

void usage() {
	fprintf(stderr,
		"hxd - tiny hex dump tool\n"
		"Usage: hxd [OPTION...] FILENAME\n"
		"  -w, --width=N\t\tchanges the width (default: 16)\n"
		"  -o, --offset=N\tchanges the starting position\n"
		"  -l, --length=N\tstops the dump after this length\n"
		"  -h, --help\t\tdisplays this help list\n"
		"\n"
		"We currently don't truncate data to meet the length exactly.\n"
		);
	exit(1);
}

int main(int argc, char** argv) {
	int opt = 0;
	while((opt = getopt_long(argc, argv, "w:o:l:h", long_options, NULL)) != -1) {
		switch(opt) {
		case 'w': g_width  = parse_int_arg("width"); break;
		case 'o': g_offset = parse_int_arg("offset"); break;
		case 'l': g_length = parse_int_arg("length"); break;
		case 'h': usage(); break;
		}
	}

	if (optind >= argc) {
		fputs("Expected a filename.\n", stderr);
		return 1;
	}

	FILE *fd = fopen(argv[optind], "r");
	if (!fd) {
		fprintf(stderr, "Could not open %s\n", argv[1]);
		return 1;
	}
	
	uint8_t buffer[g_width];

	fseek(fd, g_offset, SEEK_SET);	
	
	size_t len = 0;
	int split = g_width % 2 == 0;
	while ((len = fread(&buffer, sizeof(uint8_t), g_width, fd)) > 0) {
		// check if we have gone past our length, length is mostly a suggestion
		if (g_length > 0 && (size_t)ftell(fd) > (g_length + g_offset)) {
			break;	
		}
		
		printf("$%08lx  ", ftell(fd) - len);
		for (size_t i = 0; i < g_width; ++i) {
			if (split && i == g_width/2)
				printf(" ");

			if (i < len)
				printf("%02x ", buffer[i]);
			else 
				printf("   ");
		}
		printf(" | ");
		for (size_t i = 0; i < g_width; ++i) {
			if (i < len)
				printf("%c", buffer[i] >= ' ' && buffer[i] < 0x7f ? buffer[i] : '.');
			else
				printf(" ");
		}
		printf(" |\n");
	}
	fclose(fd);
	return 0;
}
